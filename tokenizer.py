from dataclasses import dataclass
import re

@dataclass
class Token:
    token_type: str
    value: str

class Tokenizer:
    TOKENS_TYPE = {
        'def': r'\bdef\b',
        'end': r'\bend\b',
        'identifier': r'\b[a-zA-Z]+\b',
        'oparen': r'\(',
        'cparen': r'\)',
        'integer': r'\b[0-9]+\b',
        'comma': r',',
        'plus': r'\+'
    }

    def __init__(self, text):
        self.text = text

    def tokenize(self):
        tokens = []
        while len(self.text) > 0:
            for token_type, regex in self.TOKENS_TYPE.items():
                regex = f'\A({regex})' #always matches the beginning of the string => thanks to this `end` is always last token
                match = re.search(regex, self.text)
                if match:
                    self.text = (self.text[:match.regs[0][0]] + self.text[match.regs[0][1]:]).strip()
                    tokens.append(Token(token_type, match.group()))
                    break
            else:
                raise Exception(f'Could not tokenize text {self.text}')
        return tokens