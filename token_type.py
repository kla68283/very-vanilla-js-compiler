from enum import Enum

class TokenType(Enum):
    DEF = 'def'
    END = 'end'
    IDENTIFIER = 'identifier'
    OPAREN = 'oparen'
    CPAREN = 'cparen'
    INT = 'integer'
    COMMA = 'comma'