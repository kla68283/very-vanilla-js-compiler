from tokenizer import Tokenizer
from parser import Parser
from generator import Generator


if __name__ == '__main__':
    with open('sample', 'r') as f:
        tokens = Tokenizer(f.read()).tokenize()

    tree = Parser(tokens).parse()
    code = ''
    for node in tree:
        code += Generator().generate_from(node)
    test_log = 'console.log(f(1,2));'
    code = code + test_log
    print(code)