from dataclasses import dataclass

@dataclass
class IntNode:
    value: int

@dataclass
class CallNode:
    name: str
    arg_exprs: list

@dataclass
class AdditionNode:
    arg_exprs: list

@dataclass
class VarRefNode:
    value: str

@dataclass
class DefNode:
    name: str
    arg_names: list
    body: IntNode or CallNode or VarRefNode

class Parser:
    def __init__(self, tokens):
        self.tokens = tokens

    def parse(self):
        def_nodes = []
        try:
            while True:
                def_nodes.append(self._parse_def())
        except:
            return def_nodes

    def _parse_def(self) -> DefNode:
        self._consume('def')

        name = self._consume('identifier').value
        arg_names = self._parse_arg_names()

        body = self._parse_expression()

        self._consume('end')
        return DefNode(name, arg_names, body)


    def _parse_arg_names(self) -> list:
        arg_names = []
        self._consume('oparen')
        if self._peek('identifier'):
            arg_names.append(self._consume('identifier').value)
            while self._peek('comma'):
                self._consume('comma')
                arg_names.append(self._consume('identifier').value)

        self._consume('cparen')
        return arg_names

    def _parse_expression(self):
        if self._peek('integer'):
            return IntNode(int(self._consume('integer').value))

        if self._peek('identifier') and self._peek('oparen', 1):
            return CallNode(self._consume('identifier').value, self._parse_arg_expr())

        if self._peek('identifier') and self._peek('plus', 1):
            arg_exprs = []
            arg_exprs.append(self._consume('identifier').value)
            while self._peek('plus'):
                self._consume('plus')
                arg_exprs.append(self._consume('identifier').value)
            return AdditionNode(arg_exprs)

        return VarRefNode(self._consume('identifier').value)

    def _parse_arg_expr(self):
        arg_exprs = []
        self._consume('oparen')
        if not self._peek('cparen'):
            arg_exprs.append(self._parse_expression())
            while self._peek('comma'):
                self._consume('comma')
                arg_exprs.append(self._parse_expression())

        self._consume('cparen')
        return arg_exprs

    def _consume(self, expected_type):
        token = self.tokens.pop(0)
        if token.token_type == expected_type:
            return token
        raise RuntimeError(f'Expected token type {expected_type}, but got {token.token_type}')

    def _peek(self, expected_type, offset=0):
        return self.tokens[offset].token_type == expected_type